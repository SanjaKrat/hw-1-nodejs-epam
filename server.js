const express = require('express');
const path = require('path');
const app = express();
const morgan = require('morgan');
const cors = require('cors');
const fs = require('fs');

const hostname = 'localhost';
const port = 8080;
const API_POINT = '/api/files';
const filesPath = path.join(__dirname, 'files');

app.use(express.json());
app.use(cors());
app.use(morgan('combined'));

// get all files
app.get(API_POINT, (req, res) => {
  fs.access(filesPath, (err) => {
    if (err) {
      res.statusCode = 500;
      res.send({ message: 'Server error' });
      return;
    }
  });

  fs.readdir(filesPath, (err, files) => {
    if (err) {
      res.statusCode = 500;
      res.send({ message: 'Server error' });
      return;
    }
    res.status(200).send({
      message: 'Success',
      files: files
    });
  });
});

//create file
app.post(API_POINT, (req, res) => {
  const filename = req.body.filename;
  const content = req.body.content;
  const allowedExtension = new RegExp(/^.*\.(log|txt|json|yaml|xtml|js)$/gi);
  if (!fs.existsSync(filesPath)) {
    fs.mkdir(filesPath, (err) => {
      if (err) {
        return res.status(500).send({message: "Server error"});
      }
    });
  }
  if (!filename) {
    res.statusCode = 400;
    res.send({ message: `Please specify 'filename' parameter` });
    return;
  }
  if (!content) {
    res.statusCode = 400;
    res.send({ message: `Please specify 'content' parameter` });
    return;
  }
  if (!allowedExtension.test(filename)) {
    res.statusCode = 400;
    res.send({ message: `The file extension is not allowed` });
    return;
  }
  fs.writeFile(`${filesPath}/${filename}`, content, (err) => {
    if (err) {
      res.statusCode = 500;
      res.send({ message: 'Server error' });
      return;
    }
    res.send({ message: 'File created successfully' });
  });
});

// get file by filename.extension
app.get(`${API_POINT}/:file`, (req, res) => {
  const file = req.params.file;

  fs.readFile(`files/${file}`, (err, data) => {
    if (err) {
      if (err.code === 'ENOENT') {
        res.statusCode = 400;
        res.send({ message: `No file with '${file}' filename found` });
        return;
      }
      res.statusCode = 500;
      res.send({ message: 'Server error' });
      return;
    }
    fs.stat(`files/${file}`, (err, stats) => {
      if (err) {
        res.statusCode = 500;
        res.send({ message: 'Server error' });
        return;
      }
      const uploadedDate = stats.birthtime;
      res.statusCode = 200;
      res.send({
        message: 'Success',
        filename: file,
        content: data.toString(),
        extension: path.extname(file),
        uploadedDate: uploadedDate
      });
    });
  });
});

// delete file by filename.extension
app.delete(`${API_POINT}/:file`, (req, res) => {
  const file = req.params.file;

  fs.rm(`files/${file}`, (err) => {
    if (err) {
      if (err.code === 'ENOENT') {
        res.statusCode = 400;
        res.send({ message: `No file with '${file}' filename found` });
        return;
      }
      res.statusCode = 500;
      res.send({ message: 'Server error' });
    }
    res.statusCode = 200;
    res.send({ message: 'File deleted successfully' });
  });
});

// modify file
app.patch(`${API_POINT}/:file`, (req, res) => {
  const file = req.params.file;
  const modifiedData = `\n` + req.body.content;
  if (!modifiedData) {
    res.statusCode = 400;
    res.send({ message: `Please specify 'content' parameter` });
    return;
  }
  fs.open(`files/${file}`, (err, fd) => {
    if (err) {
      if (err.code === 'ENOENT') {
        res.statusCode = 400;
        res.send({ message: `No file with '${file}' filename found` });
        return;
      }
      res.statusCode = 500;
      res.send({ message: 'Server error' });
      return;
    }
    fs.appendFile(`files/${file}`, modifiedData, (err) => {
      if (err) {
        if (err.code === 'ENOENT') {
          res.statusCode = 400;
          res.send({ message: `No file with '${file}' filename found` });
          return;
        }
        res.statusCode = 500;
        res.send({ message: 'Server error' });
      }
      fs.close(fd);
      res.statusCode = 200;
      res.send({ message: 'File modified successfully' });
    });
  });
});

app.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
